﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DebugScene : GameScene {
    public GameObject PlayerMob;
    void Start()
    {
        this.sceneName = "Debug";
        for (int x = 0; x <= 50; x++)
        {
            for (int y = 0; y <= 50; y++)
            {
                if (x % 2 == 0 && y % 2 == 0)
                {
                    GameObject curNode = GameObject.Instantiate(NodePrefab);
                    curNode.transform.position = new Vector2(x, y);
                    Nodes.Add(curNode);
                }
            }
        }


        StartCoroutine("RemoveDisableNavColliders");
        base.Start();
        PlayerMob = GameObject.FindGameObjectWithTag("Player");
        if (!gameController.FlagIntro)
        {
            StartCoroutine(CutsceneIntro());
        }
    }

    IEnumerator RemoveDisableNavColliders()
    {
        List<GameObject> NodesToRemove = new List<GameObject>();
        yield return new WaitForSeconds(2f);
        Debug.Log("Generating Nav Neighbors");
        foreach (GameObject node in Nodes)
        {
            if (node != null)
            {
                node.GetComponent<AStarNode>().GetNeighbors();
            }
            else
            {
                NodesToRemove.Add(node);
            }
        }

        Debug.Log("Removing colliding nav nodes");
        foreach (GameObject removeMe in NodesToRemove)
        {
            Nodes.Remove(removeMe);
        }

        Debug.Log("Disabling nav colliders");
        foreach (GameObject node in Nodes)
        {
            if (node != null)
            {
                node.GetComponent<CircleCollider2D>().isTrigger = true;
                node.gameObject.layer = LayerMask.NameToLayer("NavNode");
            }
        }

        yield return null;
    }

    IEnumerator CutsceneIntro()
    {
        Mob playerMob = PlayerMob.GetComponent<Mob>();
        GameObject.Destroy(PlayerMob.GetComponent<PlayerMobController>());
        CutsceneController cs = PlayerMob.AddComponent<CutsceneController>();
        playerMob.SetController(cs);
        playerMob.SetSpeed(1f);

        cs.Inputs = new Vector2(0, -1f);
        yield return new WaitForSeconds(1f);
        cs.Inputs = Vector2.zero;
        yield return gameController.Dialog(playerMob.Dialog);

        cs.Inputs = new Vector2(0, -1f);
        yield return new WaitForSeconds(3f);
        cs.Inputs = new Vector2(0, 0f);

        playerMob.Dialog.AdvanceNode();
        yield return gameController.Dialog(playerMob.Dialog);

        yield return new WaitForSeconds(2f);
        playerMob.Dialog.AdvanceNode();
        yield return gameController.Dialog(playerMob.Dialog);

        yield return new WaitForSeconds(2f);
        playerMob.Dialog.AdvanceNode();
        yield return gameController.Dialog(playerMob.Dialog);

        GameObject.Destroy(PlayerMob.GetComponent<CutsceneController>());
        PlayerMob.GetComponent<Mob>().SetController(PlayerMob.AddComponent<PlayerMobController>());
        playerMob.SetSpeed(10f);
        gameController.FlagIntro = true;
    }
}
