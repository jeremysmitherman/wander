﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeCollider : MonoBehaviour {
    Mob mob;

	// Use this for initialization
	void Start () {
        mob = transform.root.gameObject.GetComponent<Mob>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("NavNode"))
            mob.NavNodes.Add(collision.gameObject);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("NavNode"))
            mob.NavNodes.Remove(collision.gameObject);
    }
}
