﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {
    public enum WeaponType { PHYSICAL, MAGIC }
    BoxCollider2D coll;

    public WeaponType Type;
    public Mob attachedMob;

    [SerializeField]
    float potency;

    [SerializeField]
    float knockback;

    public float Potency  { get { return potency; } }
    public float Knockback { get { return knockback; } }

    // Use this for initialization
    void Start () {
        coll = gameObject.GetComponent<BoxCollider2D>();
	}
	
	// Update is called once per frame
	void Update () {

	}

    void OnTriggerEnter2D(Collider2D collision)
    {
        collision.gameObject.SendMessage("TakeHit", this, SendMessageOptions.DontRequireReceiver);
    }
}
