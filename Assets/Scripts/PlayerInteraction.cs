﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour {
    [SerializeField]
    BoxCollider2D interactionCollider;

    [SerializeField]
    List<GameObject> interactables = new List<GameObject>();

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && interactables.Count > 0)
        {
            interactables[0].GetComponent<IInteractive>().Interact();
        }
    }

    public void SetInteractionCollider(BoxCollider2D collider)
    {
        interactionCollider = collider;
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<IInteractive>() != null)
        {
            interactables.Add(other.gameObject);
        }
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        if (interactables.Contains(other.gameObject))
        {
            interactables.Remove(other.gameObject);
        }
    }
}
