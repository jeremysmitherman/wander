﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Character Sheets hold all the data about our characters.
/// </summary>
public class CharacterSheet {
    float maxHP;
    float hp;
    float maxMP;
    float mp;
    float str;
    float dex;
    float intl;    
    string characterName;
    int level;

    public float MaxHP { get { return maxHP; } }
    public float HP { get { return hp; } }
    public float MaxMP { get { return maxMP; } }
    public float MP { get { return mp; } }
    public float Str { get { return str; } }
    public float Dex { get { return dex; } }
    public float Intl { get { return intl; } }
    public string CharacterName { get { return characterName; } }
    public int Level { get { return level; } }

    public CharacterSheet(string name, int level, float hp, float maxHP, float mp, float maxMP, float str, float dex, float intl)
    {
        characterName = name;
        this.level = level;
        this.intl = intl;
        this.dex = dex;
        this.str = str;
        this.mp = mp;
        this.hp = hp;
        this.maxHP = maxHP;
        this.maxMP = maxMP;
    }
}
