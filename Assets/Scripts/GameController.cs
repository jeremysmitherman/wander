﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
    public CharacterData Leader { get { return leader; } }
    public GameObject MobPrefab;
    public GameObject PlayerPrefab;
    public Animator Fader;
    public Text DialogText;
    public bool InputDisabled;

    /// <summary>
    /// Flag Section
    /// </summary>
    public bool FlagIntro;

    /// <summary>
    /// Implement a singleton pattern for the game controller
    /// </summary>
    public static GameController instance;

    /// <summary>
    /// Character information for every character in the game
    /// </summary>
    [SerializeField]
    List<CharacterData> characters = new List<CharacterData>();

    /// <summary>
    /// Holds references to the characters currently in your party.
    /// </summary>
    [SerializeField]
    List<CharacterData> party = new List<CharacterData>();

    /// <summary>
    /// The Active character, controlled in the world
    /// </summary>
    [SerializeField]
    CharacterData leader;

    [SerializeField]
    string scene;

    float lastDialog;
    bool inDialog;
    public float DialogDelay = 1f;
    

    public List<CharacterData> Characters { get { return characters; } }
    public List<CharacterData> Party { get { return party; } }
    public string Scene { get { return scene; } }

    public void SetScene(string scenename)
    {
        this.scene = scenename;
    }

    void Awake()
    {
        if (GameController.instance != null)
        {
            GameObject.Destroy(gameObject);
        } else
        {
            GameController.instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public void SetCharacters(List<CharacterData> characterSheets)
    {
        characters = characterSheets;
    }

    public void SetParty(List<CharacterData> party)
    {
        this.party = party;
    }

    public IEnumerator RequestExit(string from, string to)
    {
        InputDisabled = true;
        Fader.SetBool("FadeOut", true);
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene(to);
        yield return new WaitForSeconds(1.5f);
        Fader.SetBool("FadeOut", false);
        yield return new WaitForSeconds(1.5f);
        InputDisabled = false;
    }

    public Coroutine Dialog(Dialog dialog)
    {
        if (!inDialog)
        {
            Debug.Log("Dialog requested");
            return StartCoroutine(DialogCo(dialog));
        }
        return null;
    }

    IEnumerator DialogCo(object dialog)
    {
        inDialog = true;
        InputDisabled = true;
        float startTime = Time.time;

        Dialog d = (Dialog)dialog;
        DialogNode node = d.CurrentNode;

        string textString = node.Text;
        int idx = 0;        
        string[] lines = textString.Split(';');
        bool userInput = false;

        string displayText;
        float delay = 0f;

        while (idx < lines.Length)
        {
            if (lines[idx].Split('-').Length > 1)
            {
                displayText = lines[idx].Split('-')[0].Replace("___", "\n");
                delay = float.Parse(lines[idx].Split('-')[1]);
            } else
            {
                displayText = lines[idx].Replace("___", "\n"); ;
                delay = 0f;
            }

            DialogText.text = displayText;
            DialogText.enabled = true;
            userInput = Input.GetKeyDown(KeyCode.Space) && Time.time > startTime + 0.2f;
            if (userInput)
            {
                idx++;
            }

            if (delay > 0)
            {
                yield return new WaitForSeconds(delay);
            } else
            {
                yield return null;
            }
        }

        if (node.NodeType == NodeType.CHOICE && node.Edges.Count > 0)
        {
            int selection = 0;

            while (true)
            {
                DialogText.text = "";
                int choiceIdx = 0;
                foreach (DialogEdge edge in node.Edges)
                {
                    if (choiceIdx == selection)
                    {
                        DialogText.text += "> ";
                    }

                    DialogText.text += edge.DialogText + "\n";
                    choiceIdx++;
                }

                if (Input.GetKeyDown(KeyCode.S))
                {
                    selection++;
                }

                if (Input.GetKeyDown(KeyCode.W))
                {
                    selection--;
                }

                if (selection > node.Edges.Count -1)
                {
                    selection = 0;
                }

                if (selection < 0)
                {
                    selection = node.Edges.Count -1;
                }

                if (Input.GetKeyDown(KeyCode.Space))
                {
                    d.CurrentNode = d.GetNodeByID(node.Edges[selection].NextNode);
                    yield return StartCoroutine(DialogCo(d));
                    break;
                }
                yield return null;
            }
        }

        if (node.NodeType == NodeType.AUTO_ADVANCE)
        {
            d.CurrentNode = d.GetNodeByID(node.Edges[0].NextNode);
            yield return StartCoroutine(DialogCo(d));
        }

        Debug.Log("Dialog Finished");
        DialogText.enabled = false;
        inDialog = false;
        InputDisabled = false;
    }

    // Use this for initialization
    void Start () {
        CharacterData salo = gameObject.AddComponent<CharacterData>();
        salo.baseHP = 110;
        salo.scaleHP = 10;
        salo.baseMP = 50;
        salo.scaleMP = 4;
        salo.baseStr = 10;
        salo.scaleStr = 10;
        salo.baseDex = 18;
        salo.scaleDex = 12;
        salo.baseIntl = 7;
        salo.scaleIntl = 12;
        salo.SetData("Salo", 1, 9999, 9999);

        SetCharacters(new List<CharacterData> { salo });
        SetParty(new List<CharacterData> { salo });
        leader = salo;

        Fader.SetBool("FadeOut", false);
    }
	
	// Update is called once per frame
	void Update () {

	}
}

