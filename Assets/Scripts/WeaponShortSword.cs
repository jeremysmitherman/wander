﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponShortSword : MonoBehaviour {
	float lightAttackHBLength = .85f;
	float lightAttackHBWidth  = .3f;
	float heavyAttackHBLength = .85f;
	float heavyAttackHBWidth  = .85f;
	float knockbackAttackHBLength = .85f;
	float knockbackAttackHBWidth  = .85f;

	public Animation HeavyAnimation;
	public Animation LightAnimation;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
