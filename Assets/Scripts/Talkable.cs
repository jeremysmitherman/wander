﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Talkable : MonoBehaviour, IInteractive {
    int node_id = 1;
    GameController controller;
    Dialog mobDialog;

	// Use this for initialization
	protected void Start () {
        controller = GameController.instance;
        mobDialog = gameObject.GetComponent<Dialog>();
        Debug.Log(mobDialog);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Interact()
    {
        controller.Dialog(mobDialog);
    }
}
