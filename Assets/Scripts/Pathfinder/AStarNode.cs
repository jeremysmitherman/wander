﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AStarNode : MonoBehaviour {
    public List<GameObject> neighbors = new List<GameObject>();
    public float FScore;
    public float GScore;
    public AStarNode Parent;

    public void GetNeighbors()
    {
        gameObject.GetComponent<CircleCollider2D>().enabled = false;
        RaycastHit2D hitN = Physics2D.Raycast(transform.position, new Vector2(0, 1), 2);
        if (hitN && hitN.collider.gameObject.GetComponent<AStarNode>() != null)
        {
            neighbors.Add(hitN.collider.gameObject);
        }

        RaycastHit2D hitS = Physics2D.Raycast(transform.position, new Vector2(0, -1), 2);
        if (hitS && hitS.collider.gameObject.GetComponent<AStarNode>() != null)
        {
            neighbors.Add(hitS.collider.gameObject);
        }

        RaycastHit2D hitE = Physics2D.Raycast(transform.position, new Vector2(1, 0), 2);
        if (hitE && hitE.collider.gameObject.GetComponent<AStarNode>() != null)
        {
            neighbors.Add(hitE.collider.gameObject);
        }

        RaycastHit2D hitW = Physics2D.Raycast(transform.position, new Vector2(-1, 0), 2);
        if (hitW && hitW.collider.gameObject.GetComponent<AStarNode>() != null)
        {
            neighbors.Add(hitW.collider.gameObject);
        }

        RaycastHit2D hitNE = Physics2D.Raycast(transform.position, new Vector2(1, 1), 2.5f);
        if (hitNE && hitNE.collider.gameObject.GetComponent<AStarNode>() != null)
        {
            neighbors.Add(hitNE.collider.gameObject);
        }

        RaycastHit2D hitNW = Physics2D.Raycast(transform.position, new Vector2(1, -1), 2.5f);
        if (hitNW && hitNW.collider.gameObject.GetComponent<AStarNode>() != null)
        {
            neighbors.Add(hitNW.collider.gameObject);
        }

        RaycastHit2D hitSW = Physics2D.Raycast(transform.position, new Vector2(-1, -1), 2.5f);
        if (hitSW && hitSW.collider.gameObject.GetComponent<AStarNode>() != null)
        {
            neighbors.Add(hitSW.collider.gameObject);
        }

        RaycastHit2D hitSE = Physics2D.Raycast(transform.position, new Vector2(1, -1), 2.5f);
        if (hitSE && hitSE.collider.gameObject.GetComponent<AStarNode>() != null)
        {
            neighbors.Add(hitSE.collider.gameObject);
        }

        gameObject.GetComponent<CircleCollider2D>().enabled = true;
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject.Destroy(gameObject);
    }
}
