﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinder {

    public List<GameObject> PathFind(GameObject SourceNode, GameObject TargetNode)
    {
        List<AStarNode> open = new List<AStarNode>();
        List<AStarNode> closed = new List<AStarNode>();
        AStarNode SourceNodeObject = SourceNode.GetComponent<AStarNode>();
        SourceNodeObject.FScore = Vector2.Distance(SourceNode.transform.position, TargetNode.transform.position);
        open.Add(SourceNodeObject);
        int cb = 0;

        while (open.Count > 0)
        {
            float lowestF = -1;
            AStarNode curNode = null;
            foreach(AStarNode node in open)
            {
                if (lowestF == -1 || node.FScore < lowestF)
                {
                    curNode = node;
                    lowestF = node.FScore;
                }
            }

            if (curNode.gameObject == TargetNode)
            {
                List<GameObject> path = new List<GameObject>();
                GameObject pathNode = curNode.gameObject;
                int cb1 = 0;
                while(pathNode != SourceNode)
                {
                    cb1++;
                    if (cb1 > 1000)
                    {
                        Debug.Log("Path gen exception");
                        break;
                    }
                    path.Add(pathNode);
                    pathNode = pathNode.GetComponent<AStarNode>().Parent.gameObject;
                }

                return path;
            }

            open.Remove(curNode);
            closed.Add(curNode);

            foreach (GameObject neighbor in curNode.neighbors)
            {
                float tenativeG = curNode.GScore + 1;
                AStarNode neighborObject = neighbor.GetComponent<AStarNode>();
                
                if (!closed.Contains(neighborObject))
                {
                    neighborObject.FScore = curNode.GScore + 1 + Vector2.Distance(neighbor.transform.position, TargetNode.transform.position);
                    if (!open.Contains(neighborObject))
                    {
                        open.Add(neighborObject);
                        neighborObject.Parent = curNode;
                    } else
                    {
                        if (tenativeG < neighborObject.GScore)
                        {
                            neighborObject.GScore = tenativeG;
                            neighborObject.Parent = curNode;
                        }        
                    }
                    
                }
            }
            cb++;
            Debug.Log(string.Format("Checked {0} Nodes", cb));
            if (cb > 10000)
            {
                Debug.Log("CB TRIPPED");
                return null;
            }
        }

        return null;
    }
}
