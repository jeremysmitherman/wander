﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMobController : MonoBehaviour, IMobController {
    public Pathfinder pf = new Pathfinder();
    public GameObject SourceNode;
    public GameObject TargetNode;

    [SerializeField]
    bool isRolling;

    [SerializeField]
    float lastRollTime;

    [SerializeField]
    Vector2 RollDirection = new Vector2();

    [SerializeField]
    Vector3 axisInputs = new Vector3();

    [SerializeField]
    Vector3 rawAxisInputs = new Vector3();

    Vector3 lastAxisInputs;

    [SerializeField]
    GameController gameController;

	[SerializeField]
	GameObject InteractionCollider;

    [SerializeField]
    bool canStrongAttack = true;

	[SerializeField]
	Mob playerMob;

    public GameObject MeleeCollider;
    float lastFireTime;
    Animator animator;
    List<GameObject> path = null;


    // Use this for initialization
    void Start () {
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        animator = GetComponent<Animator>();
		playerMob = gameObject.GetComponent<Mob> ();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            path = pf.PathFind(SourceNode, TargetNode);
        }

        if (path != null)
        {
            foreach (GameObject pn in path)
            {
                if (pn.GetComponent<AStarNode>().Parent != null)
                {
                    Debug.DrawLine(pn.transform.position, pn.GetComponent<AStarNode>().Parent.transform.position, Color.red);
                }
            }
        }

        if (Input.GetButtonDown("Jump"))
        {
            if (!isRolling && Time.time > lastRollTime + 1.2f)
            {
				Debug.Log ("Starting Roll");
                isRolling = true;
				RollDirection.x = Input.GetAxisRaw("Horizontal");
				RollDirection.y = Input.GetAxisRaw("Vertical");
				lastRollTime = Time.time;
            }
        }

        if (isRolling && Time.time > lastRollTime + .1f)
        {
			Debug.Log ("Ending Roll");
            isRolling = false;
        }

        if (Input.GetButtonDown("Fire1"))
        {
            lastFireTime = Time.time;
        } else
        {
            if (canStrongAttack && Input.GetButton("Fire1") && Time.time - lastFireTime > .5f)
            {
                animator.SetTrigger("HeavyAttack");
                canStrongAttack = false;
            }
        }

        if (Input.GetButtonUp("Fire1"))
        {
            canStrongAttack = true;
            if (Time.time - lastFireTime < .5f)
            {
                animator.SetTrigger("LightAttack");
            }
        }
       
		if (!isRolling) {
			playerMob.SetSpeed (5);
			axisInputs.x = Input.GetAxis ("Horizontal");
			axisInputs.y = Input.GetAxis ("Vertical");
			//axisInputs.z = Input.GetAxis("Jump");

			rawAxisInputs.x = Input.GetAxisRaw ("Horizontal");
			rawAxisInputs.y = Input.GetAxisRaw ("Vertical");
			//rawAxisInputs.z = Input.GetAxisRaw("Jump");

			// Calculate facing if mob wasn't moving last frame
			if (lastAxisInputs == Vector3.zero || axisInputs.magnitude == 1) {
				if (axisInputs.x > 0) {
					playerMob.SetFacing (Mob.Facing.RIGHT);
					MeleeCollider.transform.eulerAngles = new Vector3 (0, 0, 270);
				}

				if (axisInputs.x < 0) {
					playerMob.SetFacing (Mob.Facing.LEFT);
					MeleeCollider.transform.eulerAngles = new Vector3 (0, 0, 90);
				}

				if (axisInputs.y > 0) {
					playerMob.SetFacing (Mob.Facing.UP);
					MeleeCollider.transform.eulerAngles = new Vector3 (0, 0, 0);
				}

				if (axisInputs.y < 0) {
					playerMob.SetFacing (Mob.Facing.DOWN);
					MeleeCollider.transform.eulerAngles = new Vector3 (0, 0, 180);
				}
			}
			lastAxisInputs = rawAxisInputs;
	        

			Mob.Facing facing = playerMob.GetFacing ();
			if (facing == Mob.Facing.UP) {
				InteractionCollider.transform.eulerAngles = new Vector3 (0, 0, 0);
			}

			if (facing == Mob.Facing.DOWN) {
				InteractionCollider.transform.eulerAngles = new Vector3 (0, 0, 180);
			}

			if (facing == Mob.Facing.LEFT) {
				InteractionCollider.transform.eulerAngles = new Vector3 (0, 0, 90);
			}

			if (facing == Mob.Facing.RIGHT) {
				InteractionCollider.transform.eulerAngles = new Vector3 (0, 0, 270);
			}
		} else {
			rawAxisInputs = RollDirection;
			playerMob.SetSpeed (25);
		}

    }

    void FixedUpdate()
    {
        // Actually move the mob based on the inputs gathered in Update.
        transform.Translate(rawAxisInputs * playerMob.Speed * playerMob.SpeedFactor * Time.deltaTime);
    }

    public Vector3 GetAxisInputs()
    {
        return axisInputs;
    }

    public Vector3 GetRawAxisInputs()
    {
        return (gameController.InputDisabled) ? Vector3.zero : rawAxisInputs;
    }

    public float GetFireAxis()
    {
        return Input.GetAxisRaw("Fire1");
    }
}
