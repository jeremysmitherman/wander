﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowLeaderController : MonoBehaviour, IMobController {
    /// <summary>
    /// The target for this mob to follow
    /// </summary>
    GameObject target;

    /// <summary>
    /// How much leeway does this character have to roam from the target?
    /// </summary>
    float precision;

    /// <summary>
    /// How long does it take for this mob to start following a new route?
    /// </summary>
    float delay;

    public void SetTarget(GameObject target)
    {
        this.target = target;
    }

    public void SetPrecision(float precision)
    {
        this.precision = precision;
    }

    public Vector3 GetAxisInputs()
    {
        throw new NotImplementedException();
    }

    public Vector3 GetRawAxisInputs()
    {
        /*if (Vector2.Distance(target.transform.position, transform.position) > 2f)
        {
            return Vector2.MoveTowards(transform.position, (Vector2)target.transform.position, 1f);
        } else
        {
            return Vector2.zero;
        }*/

        return Vector2.zero;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Vector2.Distance(transform.position, target.transform.position) > precision) {
            transform.position = Vector2.MoveTowards(transform.position, (Vector2)target.transform.position, .1f);
        }
    }

    public float GetFireAxis()
    {
        throw new NotImplementedException();
    }
}
