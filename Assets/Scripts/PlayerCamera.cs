﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour {
    public GameObject Target;
    Vector3 targetPos;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        if (Target == null)
        {
            Target = GameObject.FindGameObjectWithTag("Player");
        }
        else
        {
            targetPos.x = Target.transform.position.x;
            targetPos.y = Target.transform.position.y;
            targetPos.z = -10f;

            transform.position = targetPos;
        }
	}
}
