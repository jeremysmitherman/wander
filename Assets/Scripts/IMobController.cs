﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMobController {
    Vector3 GetAxisInputs();
    Vector3 GetRawAxisInputs();
    float GetFireAxis();
}
