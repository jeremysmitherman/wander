﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameScene : MonoBehaviour {
    protected GameController gameController;
    protected string sceneName;
    public GameObject NodePrefab;
    public List<GameObject> Nodes;

    public string SceneName { get { return sceneName; } }

    // Use this for initialization
    protected void Start () {
        gameController = GameController.instance;
        InitScene();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void InitScene()
    {
        GameObject Leader = null;
        List<GameObject> mobs = new List<GameObject>();
        foreach (CharacterData character in gameController.Party)
        {
            GameObject mob = GameObject.Instantiate(gameController.PlayerPrefab);
            mob.GetComponent<Mob>().CharData = character;

            if (character.CharacterName == "Salo")
            {
                mob.GetComponent<Mob>().SetDialog(new JersDialog());
            }
            try {
                mob.transform.position = GameObject.Find(string.Format("EntranceFrom{0}", gameController.Scene)).transform.position;
            }catch{
                mob.transform.position = new Vector3(0, 8, 0);
                Debug.Log(string.Format("No object named EntranceFrom{0}", gameController.Scene));
            }
            if (gameController.Leader == character)
            {
                Leader = mob;
                mob.tag = "Player";
            }
        }

        gameController.SetScene(this.sceneName);

        int idx = 1;
        foreach (GameObject mob in mobs)
        {
            if (mob != Leader)
            {
                FollowLeaderController followController = mob.AddComponent<FollowLeaderController>();
                followController.SetTarget(Leader);
                followController.SetPrecision(idx * 2f);
                mob.GetComponent<Mob>().SetController(followController);
                idx++;
            }
        }
    }
}
