﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exit : MonoBehaviour {
    GameController controller;
    GameScene scene;

    [SerializeField]
    string exitTo;

	// Use this for initialization
	void Start () {
        controller = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        scene = GameObject.FindGameObjectWithTag("SceneController").GetComponent<GameScene>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log(string.Format("Requesting exit from {0} to {1}", scene.SceneName, exitTo));
            controller.StartCoroutine(controller.RequestExit(scene.SceneName, exitTo));
        }
    }
}
