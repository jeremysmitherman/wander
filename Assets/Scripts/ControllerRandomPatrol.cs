﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerRandomPatrol : MonoBehaviour, IMobController {
    [SerializeField]
    float delayMin;

    [SerializeField]
    float delayMax;

    Vector2 lastInput;

    [SerializeField]
    float lastMove;

    [SerializeField]
    float moveTime;

    [SerializeField]
    float lastDelayTime;

    [SerializeField]
    float delayTime;

    public Vector3 GetAxisInputs()
    {
        throw new NotImplementedException();
    }

    public float GetFireAxis()
    {
        throw new NotImplementedException();
    }

    public Vector3 GetRawAxisInputs()
    {
        if (Time.time < lastDelayTime + delayTime)
        {
            return Vector3.zero;
        }

        if (Time.time > moveTime + lastMove)
        {
            lastDelayTime = Time.time;
            delayTime = UnityEngine.Random.Range(delayMin, delayMax);

            lastMove = Time.time + delayTime;
            moveTime = UnityEngine.Random.Range(.2f, 1f);
            lastInput = new Vector2(UnityEngine.Random.Range(-1f, 1f), UnityEngine.Random.Range(-1f, 1f));
        }
        return lastInput;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
