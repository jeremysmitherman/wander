﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum NodeType { NORMAL, AUTO_ADVANCE, END_NODE, CHOICE }

public class DialogNode
{
    public int NodeID { get; }
    public List<DialogEdge> Edges = new List<DialogEdge>();
    public string Text;
    public NodeType NodeType = NodeType.NORMAL;

    public DialogNode(int node_id)
    {
        NodeID = node_id;
    }
}

public class DialogEdge
{
    public int PrevNode;
    public int NextNode;
    public string DialogText;
}

public class Dialog : MonoBehaviour {
    public DialogNode CurrentNode;
    public List<DialogNode> Nodes = new List<DialogNode>();

    public void Start()
    {
        CurrentNode = GetNodeByID(1);
    }

    public DialogNode GetNodeByID(int id)
    {
        foreach(DialogNode node in Nodes)
        {
            if (id == node.NodeID)
            {
                return node;
            }
        }
        return null;
    }

    public DialogNode AdvanceNode()
    {
        if (CurrentNode.Edges.Count == 0)
        {
            CurrentNode = Nodes[Nodes.IndexOf(CurrentNode) + 1];
            return CurrentNode;
        } else
        {
            return null;
        }
    }
}
