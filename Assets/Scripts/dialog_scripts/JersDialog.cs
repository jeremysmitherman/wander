﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JersDialog : Dialog {

	// Use this for initialization
	public JersDialog() {
        DialogNode n_01 = new DialogNode(1);
        n_01.Text = "Ugh... If I don't find some work soon, I'm not too sure how I'm gonna eat tonight...";

        DialogNode n_02 = new DialogNode(2);
        n_02.Text = "The mood here is pretty tense... I wonder what's going on.";

        DialogNode n_03 = new DialogNode(3);
        n_03.Text = "Townspeople: \"Another one went missing last night too.\";Townspeople: \"I don't understand.... who could be doing this?!\"";

        DialogNode n_04 = new DialogNode(4);
        n_04.Text = "Sounds pretty rough...;Well it's none of my concern, I better check the local shops and see if anyone needs a hand.";

        Nodes.Add(n_01);
        Nodes.Add(n_02);
        Nodes.Add(n_03);
        Nodes.Add(n_04);
        CurrentNode = n_01;
	}
}
