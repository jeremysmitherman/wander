﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopkeeperDialog : Dialog {

    // Use this for initialization
    public void Start()
    {
        DialogNode n_01 = new DialogNode(1);
        n_01.Text = "Shopkeeper: Hey, welcome.  I know we're a little um, low on stock at the moment but I'm sure we can find something for you.";
        n_01.NodeType = NodeType.CHOICE;
        DialogEdge edge1 = new DialogEdge();
        edge1.DialogText = "It's a real shame I'm not in the market for dust...";
        edge1.PrevNode = 1;
        edge1.NextNode = 2;
        DialogEdge edge2 = new DialogEdge();
        edge2.DialogText = "I was actually hoping to find some work.";
        edge2.PrevNode = 1;
        edge2.NextNode = 4;
        DialogEdge edge3 = new DialogEdge();
        edge3.DialogText = "What can you tell me about the people going missing at night?";
        edge3.PrevNode = 1;
        edge3.NextNode = 3;
        n_01.Edges = new List<DialogEdge>{ edge1, edge2, edge3 };

        DialogNode n_02 = new DialogNode(2);
        n_02.Text = "Shopkeeper: Yeah yeah, I realize how shabby this looks. But with everything going on here lately...";
        DialogEdge edge_2_1 = new DialogEdge();
        edge_2_1.DialogText = "Yeah, seems like everyone is on edge.";
        edge_2_1.PrevNode = 2;
        edge_2_1.NextNode = 3;
        DialogEdge edge_2_2 = new DialogEdge();
        edge_2_2.DialogText = "You talking about the missing people?";
        edge_2_2.PrevNode = 2;
        edge_2_2.NextNode = 3;
        n_02.Edges = new List<DialogEdge> { edge_2_1, edge_2_2 };

        DialogNode n_03 = new DialogNode(3);
        n_03.Text = "Shopkeeper: Oh, so you heard about that huh.";
        n_03.NodeType = NodeType.AUTO_ADVANCE;
        DialogEdge edge_3_1 = new DialogEdge();
        edge_3_1.PrevNode = 3;
        edge_3_1.NextNode = 5;
        n_03.Edges = new List<DialogEdge> { edge_3_1 };

        DialogNode n_04 = new DialogNode(4);
        n_04.Text = "Shopkeeper: Well, here's the thing.  Normally I wouldn't.  I try to run lean and that means just me.  But....";
        n_04.NodeType = NodeType.AUTO_ADVANCE;
        DialogEdge edge_4_1 = new DialogEdge();
        edge_4_1.PrevNode = 4;
        edge_4_1.NextNode = 5;
        n_04.Edges = new List<DialogEdge> { edge_4_1 };

        DialogNode n_05 = new DialogNode(5);
        n_05.NodeType = NodeType.END_NODE;
        n_05.Text = "Shopkeeper: This village has a group of hunters that collects pelts and brings them back to us.  We generally had a pretty good thing going;Shopkeeper: They'd bring me furs, I'd make them armor, and sell the rest here at the store.;Shopkeeper: Lately though... at night... the hunters have been going missing.  Straight vanished, without a sound.;Shopkeeper: The rest of the saps wake up and there'll be nothing left of one of their friends but a bedroll and no witnesses...;Shopkeeper: Now there's lots of theories going around and not to be cold but that ain't important to me,  running off to the north, or captured by vampires, none of my concern.;Jers: Wait, wait.  Did you say vampires?;Shopkeeper: Don't pay it no heed, fairy tales, fear, and boredom lead to lots of fanciful stories getting thrown around.;Shopkeeper: Anyway, the hunters are getting spooked and most of 'em have decided that they'd rather be househusbands or something equally awful, and so I can't get my hands on no fur and no hides.  No fur and no hides means I can't make new armor.  Armor is how I make my money, see the problem?;Jers: Ok, I think I see where this is going.;Shopkeeper: I need someone to go outside the city and bring me some pelts.  The wolves lurking outside the walls should do fine.;Jers: I think I can handle that.;Shopkeeper: Good, Three should do, but bring me five and I'll throw in a bonus for ya.  And here, take this and consider it an advance.  Go to the potion shop next door and get some medicine;Shopkeeper: It won't do no good for either of us if you get yourself killed.";

        Nodes.Add(n_01);
        Nodes.Add(n_02);
        Nodes.Add(n_03);
        Nodes.Add(n_04);
        Nodes.Add(n_05);
        CurrentNode = n_01;
        base.Start();
    }
}
