﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mob : MonoBehaviour {
	public enum Facing {UP, DOWN, LEFT, RIGHT}
    public GameObject NavNode;

    [SerializeField]
    public List<GameObject> NavNodes = new List<GameObject>();

    /// <summary>
    /// The class provding inputs for this mob.  Usually a player or AI controller.
    /// </summary>
    [SerializeField]
    IMobController controller;

    /// <summary>
    /// The rate at which a mob moves per tick.
    /// </summary>
    [SerializeField]
    float speed = 10;
    public float Speed { get { return speed; } }

    ///<summary>
    /// Multiplied into speed for final speed value.  Useful for speeding up or slowing down by a factor.
    /// </summary>
    [SerializeField]
    float speedFactor = 1f;
    public float SpeedFactor { get { return speedFactor; } set { speedFactor = value; } }

    /// <summary>
    /// The directional input provided by a mob controller.
    /// </summary>
    [SerializeField]
    Vector3 axisInputs;

	/// <summary>
	/// The directional input provided by a mob controller on the last update.  Used to help with facing.
	/// </summary>
	[SerializeField]
	Vector3 lastAxisInputs;

    [SerializeField]
    float fireAxis;

	/// <summary>
	/// The direction this mob is facing
	/// </summary>
	[SerializeField]
	Facing facing;

    /// <summary>
    /// Collision component for this mob.  Dynamically found at start
    /// </summary>
    Collider2D mobCollider;

    /// <summary>
    /// Dialog object
    /// </summary>
    Dialog dialog;

    public CharacterData CharData;

    [SerializeField]
    Animator animator;

    public Dialog Dialog { get { return dialog; } }

    public void SetDialog(Dialog dialog)
    {
        this.dialog = dialog;
    }

    public void SetController(IMobController controller)
    {
        this.controller = controller;
    }

    public void SetSpeed(float speed)
    {
        this.speed = speed;
    }

	public Facing GetFacing() 
	{
		return facing;
	}

	// Use this for initialization
	void Start () {
        if (controller == null)
        {
            controller = gameObject.GetComponent<IMobController>();
        }
        animator = gameObject.GetComponent<Animator>();
        mobCollider = gameObject.GetComponent<Collider2D>();
	}
	
	// Update is called once per frame
	void Update () {
        // Check for death
        if (CharData.HP <= 0)
        {
            HandleDeath();
        }

        GameObject closestDistance = null;
        foreach (GameObject node in NavNodes)
        {
            float nodeDist = Vector2.Distance(node.transform.position, transform.position);
            if (closestDistance == null || nodeDist < Vector2.Distance(closestDistance.transform.position, transform.position))
            {
                closestDistance = node;
            }
        }

        if (closestDistance != null)
        {
            NavNode = closestDistance;
        }
    }


    void HandleDeath()
    {
        if (GameController.instance.Leader != gameObject.GetComponent<CharacterData>())
        {
            foreach (CharacterData data in GameController.instance.Party)
            {
                Debug.Log(string.Format("Setting XP for {0} to {1}", data.CharacterName, data.XP + CharData.XPWorth));
                data.SetXP(data.XP + CharData.XPWorth);
            }

            GameObject.Destroy(gameObject);
        }
    }

    public void TakeHit(object Weapon)
    {
        Debug.Log("TOOK HIT");
        Weapon w = (Weapon)Weapon;
        float dmg = Mathf.Ceil(w.Potency - Mathf.Ceil(CharData.Dex / 8) - Mathf.Ceil((CharData.Str / 2)));
        CharData.SetHP(CharData.HP - dmg);
        Debug.Log(string.Format("Did {0} dmg", dmg));
        Debug.Log(CharData.HP);
    }

    public void SetFacing(Facing newFacing)
    {
        facing = newFacing;
    }
}
