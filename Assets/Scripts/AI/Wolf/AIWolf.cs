﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIWolf : MonoBehaviour, IMobController {
    public Detection DetectionObj;
    public GameObject MeleeCollider;
    public GameObject MeleeColliderHeavy;
    Animator animator;
    float lastAttack;
    float attackDelay;
    Mob controlledMob;
    public bool isHeavyAttacking;
	float decisionLoopTime;
	float lastDecisionTime;
	bool isCreeping;
	Vector2 creepPos;

    public Vector3 GetAxisInputs()
    {
        throw new NotImplementedException();
    }

    public float GetFireAxis()
    {
        throw new NotImplementedException();
    }

    public Vector3 GetRawAxisInputs()
    {
        return Vector3.zero;
    }

    // Use this for initialization
    void Start () {
        animator = gameObject.GetComponent<Animator>();
        controlledMob = gameObject.GetComponent<Mob>();
        attackDelay = 1.5f;
		decisionLoopTime = 3f;
	}
	
	// Update is called once per frame
	void Update () {
        float speed;
		if (isCreeping) 
		{
			transform.position = Vector2.MoveTowards(transform.position, creepPos, 0.8f * Time.deltaTime);
		}

        if (DetectionObj.Leashing)
        {

        }

        if (DetectionObj.Player != null)
        {
			if (DetectionObj.PlayerDetected) {
				if (Vector2.Distance (transform.position, DetectionObj.Player.transform.position) < 2f) {
					if (Time.time > decisionLoopTime + lastDecisionTime) {
						if (UnityEngine.Random.Range (0f, 1f) > .5f) {
							isCreeping = false;
							Debug.Log ("Wolf has decided to Attack!");
							if (Time.time > lastAttack + attackDelay) {
								if (UnityEngine.Random.Range (0f, 1f) > .8f) {
									animator.SetTrigger ("HeavyAttack");
									attackDelay = 2f;
								} else {
									animator.SetTrigger ("LightAttack");
									attackDelay = 1f;
								}

								lastAttack = Time.time;
							}
						} else {
							Debug.Log ("Wolf has decided to creep...");
							isCreeping = true;
							creepPos = UnityEngine.Random.insideUnitCircle * 2f + (Vector2)transform.position;
						}
						lastDecisionTime = Time.time;
					}
				}
                else
                {
                    speed = (Vector2.Distance(transform.position, DetectionObj.Player.transform.position) > 4f) ? 4f : 2f;
                    if (Time.time > lastAttack + attackDelay)
                    {
                        transform.position = Vector2.MoveTowards(transform.position, DetectionObj.Player.transform.position, speed * Time.deltaTime);
                        Vector2 targetDir = (DetectionObj.Player.transform.position - transform.position).normalized;
                        if (Mathf.Abs(targetDir.x) > Mathf.Abs(targetDir.y))
                        {
                            if (targetDir.x > 0)
                            {
                                controlledMob.SetFacing(Mob.Facing.RIGHT);
                            }
                            else
                            {
                                controlledMob.SetFacing(Mob.Facing.LEFT);
                            }
                        }
                        else
                        {
                            if (targetDir.y > 0)
                            {
                                controlledMob.SetFacing(Mob.Facing.UP);
                            }
                            else
                            {
                                controlledMob.SetFacing(Mob.Facing.DOWN);
                            }
                        }

                        switch (controlledMob.GetFacing())
                        {
                            case Mob.Facing.RIGHT:
                                MeleeCollider.transform.eulerAngles = new Vector3(0, 0, 270);
                                MeleeColliderHeavy.transform.eulerAngles = new Vector3(0, 0, 270);
                                break;
                            case Mob.Facing.LEFT:
                                MeleeCollider.transform.eulerAngles = new Vector3(0, 0, 90);
                                MeleeColliderHeavy.transform.eulerAngles = new Vector3(0, 0, 270);
                                break;
                            case Mob.Facing.UP:
                                MeleeCollider.transform.eulerAngles = new Vector3(0, 0, 0);
                                MeleeColliderHeavy.transform.eulerAngles = new Vector3(0, 0, 270);
                                break;
                            case Mob.Facing.DOWN:
                                MeleeCollider.transform.eulerAngles = new Vector3(0, 0, 180);
                                MeleeColliderHeavy.transform.eulerAngles = new Vector3(0, 0, 270);
                                break;
                        }
                    }
                }
            }
        }
	}
}
