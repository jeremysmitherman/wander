﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Detection : MonoBehaviour {
    public GameObject Player;

    [SerializeField]
    Vector2 detectedPostiion;

    public GameObject DetectedNode;

    [SerializeField]
    Vector2 initialPos;

    [SerializeField]
    float distanceToPlayer;

    public float LeashDistance;
    public bool PlayerDetected = false;
    public bool Leashing;
    LayerMask mask;

	// Use this for initialization
	void Start () {
        initialPos = transform.position;
        Player = GameObject.FindGameObjectWithTag("Player");
        mask = LayerMask.GetMask("Player", "Default");
	}
	
	// Update is called once per frame
	void Update () {
        if (Player == null)
        {
            Player = GameObject.FindGameObjectWithTag("Player");
        }
        else
        {
            distanceToPlayer = Vector2.Distance(transform.position, Player.transform.position);
            RaycastHit2D hit = Physics2D.Raycast(transform.position, (Player.transform.position - transform.position).normalized, 5f, mask);

            if (!Leashing && hit && hit.collider.gameObject.tag == "Player")
            {
                PlayerDetected = true;
                DetectedNode = Player.GetComponent<Mob>().NavNode;
            }

            if (!Leashing && PlayerDetected && Vector2.Distance(detectedPostiion, Player.transform.position) > LeashDistance)
            {
                PlayerDetected = false;
                Leashing = true;
            }

            if (Leashing && Vector2.Distance(transform.position, initialPos) < 3f)
            {
                Leashing = false;
            }
        }
	}
}
