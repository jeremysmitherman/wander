﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// CharacterData is the vehicle we'll use to deliver character stats to the game engine.
/// </summary>
public class CharacterData : MonoBehaviour {
    public float baseHP;
    public float baseMP;
    public float baseStr;
    public float baseDex;
    public float baseIntl;

    public float scaleHP;
    public float scaleMP;
    public float scaleStr;
    public float scaleDex;
    public float scaleIntl;

    [SerializeField]
    float maxHP;

    [SerializeField]
    float hp;

    [SerializeField]
    float maxMP;

    [SerializeField]
    float mp;

    [SerializeField]
    float str;

    [SerializeField]
    float dex;

    [SerializeField]
    float intl;

    [SerializeField]
    string characterName;

    [SerializeField]
    int level;

    [SerializeField]
    int xp;

    [SerializeField]
    int xpWorth;

    public float MaxHP { get { return maxHP; } }
    public float HP { get { return hp; } }
    public float MaxMP { get { return maxMP; } }
    public float MP { get { return mp; } }
    public float Str { get { return str; } }
    public float Dex { get { return dex; } }
    public float Intl { get { return intl; } }
    public string CharacterName { get { return characterName; } }
    public int Level { get { return level; } }
    public int XP { get { return xp; } }
    public int XPWorth { get { return xpWorth; } }

    public void SetData(string name, int level, float hp, float mp)
    {
        characterName = name;
        this.level = level;
        this.intl = (baseIntl + scaleIntl * Mathf.Pow(level, 0.125f) * (level / 16));
        this.dex = (baseDex + scaleDex * Mathf.Pow(level, 0.25f) * (level / 16));
        this.str = (baseStr + scaleStr * Mathf.Pow(level, 0.5f) * (level / 16));
        this.maxMP = (baseMP + scaleMP * (100 + level / 20) * level / 8);
        this.maxHP = (baseHP + scaleHP * (100 + level / 20) * level / 8);
        this.hp = Mathf.Clamp(hp,0,MaxHP);
        this.mp = Mathf.Clamp(mp, 0, MaxMP);
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetHP(float newHP)
    {
        hp = newHP;
    }

    public void SetXP(int newXP)
    {
        xp = newXP;
    }
}
